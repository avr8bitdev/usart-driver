/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "USART_functions.h"
#include "../Peripherals_addresses.h"
#include "../DIO_driver/DIO_functions.h"
#include "../INT_driver/INT_functions.h"
#include "../../basic_includes/bit_manip.h"
#include <avr/interrupt.h>


// --- internal --- //
#define USART_CB_LENGTH_Tx_COMPLETE 5
#define USART_CB_LENGTH_Tx_READY    5
#define USART_CB_LENGTH_Rx_COMPLETE 5

static USART_CB_Tx_t USART_CB_Queue_Tx_Complete[USART_CB_LENGTH_Tx_COMPLETE] = {0};
static USART_CB_Tx_t USART_CB_Queue_Tx_Ready[USART_CB_LENGTH_Tx_READY]       = {0};
static USART_CB_Rx_t USART_CB_Queue_Rx_Complete[USART_CB_LENGTH_Rx_COMPLETE] = {0};

static inline u8 USART_u8GetReg_USART_CTRL_STATUS_C(void)
{
	// see ATmega32 datasheet p157 for info about this.
	// 2 immediate and consecutive read cycles returns 'USART_CTRL_STATUS_C'.

	register u8 usart_ctrl_status_c_;

	// disable interrupts
	volatile u8 mcu_status_b7 = (MCU_STATUS & 0b10000000);
	INT_vidDisable_global_flag();

	asm volatile (
			"in %0,0x20 \n\t"  // cycle 1: get 'BAUD_RATE_HBYTE'
			"in %0,0x20 \n\t"  // cycle 2: get 'USART_CTRL_STATUS_C'

			:"=r"(usart_ctrl_status_c_) // output is a register (represented by 'usart_ctrl_status_c_')
			:// no input parameters
			 	 );

	// restore interrupts to original state
	MCU_STATUS |= mcu_status_b7;

	return usart_ctrl_status_c_;
}

static void USART_vidChangeBit_USART_CTRL_STATUS_C(const u8 u8PinNCpy, const u8 u8StateCpy)
{
	u8 usart_ctrl_status_c_ = USART_u8GetReg_USART_CTRL_STATUS_C();

	BIT_SET(usart_ctrl_status_c_, 7); // register select: 'USART_CTRL_STATUS_C' (this is useless)

	BIT_ASSIGN(usart_ctrl_status_c_, u8PinNCpy, u8StateCpy);

	// change bit
	USART_CTRL_STATUS_C_AND_BAUD_RATE_HBYTE = usart_ctrl_status_c_;

}

static u8 USART_u8is9bitData(void)
{
	if (BIT_GET(USART_CTRL_STATUS_B, 2) == BIT_GET(USART_data_size_9bits, 2))
	{
		register u8 usart_ctrl_status_c_ = USART_u8GetReg_USART_CTRL_STATUS_C();

		if (
			    ( BIT_GET(usart_ctrl_status_c_, 2) == BIT_GET(USART_data_size_9bits, 1) )
             && ( BIT_GET(usart_ctrl_status_c_, 1) == BIT_GET(USART_data_size_9bits, 0) )
		   )
			return 1;
	}

	return 0;
}
// ---------------- //


void USART_vidInit(USART_data_sz_t enumDataSzCpy, USART_parity_t enumParityModeCpy,
		           USART_stopBit_t enumStopBitSzCpy,USART_baud_clk_t enumBaudClkCpy,
				   USART_synch_t enumUSARTsynchModeCpy)
{
	// By Trial and Error, the below line results in corrupted transmission
	//                     on the first transmitted data unit.
	//DIO_vidSet_pinDirection(port_D, 1, OUTPUT); // Tx pin

    USART_vidDisableRx();
    USART_vidDisableTx();

	USART_vidIsTxSpeed_x2(0); // disable 2x Tx speed

	USART_vidIsMultiProcessorComm(0); // disable multi-processor communication

	USART_vidChangeSynchMode(enumUSARTsynchModeCpy);

	USART_vidChangeDataSize(enumDataSzCpy);

	USART_vidChangeParityMode(enumParityModeCpy);

	USART_vidChangeStopBitMode(enumStopBitSzCpy);

	USART_vidChangeBaudRate(enumBaudClkCpy);

	// reset callback functions
	for (u8 i = 0; i < USART_CB_LENGTH_Tx_COMPLETE; i++)
		USART_CB_Queue_Tx_Complete[i] = 0;

	for (u8 i = 0; i < USART_CB_LENGTH_Tx_READY; i++)
		USART_CB_Queue_Tx_Ready[i] = 0;

	for (u8 i = 0; i < USART_CB_LENGTH_Rx_COMPLETE; i++)
		USART_CB_Queue_Rx_Complete[i] = 0;

    // deactivate internal pull-ups
    DIO_vidSet_pinDirection(port_D, 0, INPUT); // Rx
    DIO_vidDeactivate_pinPullUp(port_D, 0);
    DIO_vidSet_pinDirection(port_D, 1, INPUT); // Tx
    DIO_vidDeactivate_pinPullUp(port_D, 1);

	USART_vidEnableTx(); // enable Tx
	USART_vidEnableRx(); // enable Rx
}

void USART_vidTransmitData(const u16 u16ValueCpy)
{
	// trap until Tx buffer is ready to accept data
	while (!USART_u8isTxReadyToAcceptData())
	{}

	if (USART_u8is9bitData()) // if 9-bit data
	{
		BIT_ASSIGN( USART_CTRL_STATUS_B, 0, BIT_GET(u16ValueCpy, 8) );
	}

	USART_RX_TX_DATA = (u8)u16ValueCpy;

	// when 'Tx Complete' interrupt is enabled, the 'Transmit Complete' bit stays 0
	// until the ISR returns, hence rendering the below while() statement infinite
	if (!USART_u8isTxCompleteINTenabled())
	{
		// trap until all data are shifted out
		while (!USART_u8isTxBufferFinished())
		{}

		BIT_SET(USART_CTRL_STATUS_A, 6); // clear 'Transmit Complete' bit by writing 1 to it
	}

}

void USART_vidTransmitStr(const char* charPtrStrCpy)
{
	while (*charPtrStrCpy)
	{
		USART_vidTransmitData(*charPtrStrCpy);

		charPtrStrCpy++;
	}
}

void USART_vidTransmitInt(s32 s32NumCpy)
{
	s8 indexer = 0;

	if (s32NumCpy < 0)
	{
		s32NumCpy *= -1;
		USART_vidTransmitData('-');
	}

	u8 digits_buffer[10]; // 2^32 = 4294967296 (10 digits max)

	// store in buffer
	do
	{
		digits_buffer[indexer++] = (s32NumCpy % 10) + '0';
	}
	while (s32NumCpy /= 10);

	// write int from array backwards
	for (indexer--; indexer >= 0; indexer--)
		USART_vidTransmitData( digits_buffer[indexer] );
}

void USART_vidTransmitFloat(f32 f32NumCpy, u8 u8AccuracyCpy)
{
	u8 isNegative = 0;

	if (f32NumCpy < 0)
	{
		isNegative = 1;
		f32NumCpy *= -1;
	}

	u32 int_part = (u32)f32NumCpy; // int part
	f32NumCpy -= int_part;         // float part

	// this prevents writing '-' if no accuracy required and int_part = 0
	if (isNegative)
	{
		if (int_part)
			USART_vidTransmitData('-');
		else if (u8AccuracyCpy && f32NumCpy)
			USART_vidTransmitData('-');
	}

	USART_vidTransmitInt(int_part);

	// return if no accuracy required or no floating part
	if (!u8AccuracyCpy || !f32NumCpy)
		return;

	USART_vidTransmitData('.');

	// while there's is accuracy and floating part
	while (u8AccuracyCpy-- && f32NumCpy)
	{
		f32NumCpy *= 10; // move digit before floating point
		const u8 current_digit = (u8)f32NumCpy;
		USART_vidTransmitData(current_digit + '0');
		f32NumCpy -= current_digit; // remove digit before floating point
	}

}

void USART_vidTransmitDataAsBits(const u16 u16NumCpy)
{
	if (USART_u8is9bitData()) // if 9-bit data
		USART_vidTransmitData( BIT_GET(u16NumCpy, 8) + '0' );

	for (s8 i = sizeof(u8)*8 - 1; i >= 0; i--)
	{
		USART_vidTransmitData( BIT_GET((u8)u16NumCpy, i) + '0' );
	}
}

// ------ setters ------//
// *** buffers ***//
inline void USART_vidEnableTx(void)
{
	BIT_SET(USART_CTRL_STATUS_B, 3);
}

inline void USART_vidDisableTx(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_B, 3);
}

inline void USART_vidEnableRx(void)
{
	BIT_SET(USART_CTRL_STATUS_B, 4);
}

inline void USART_vidDisableRx(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_B, 4);
}

// *** error flags ***//
inline void USART_vidResetIsFrameError(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_A, 4);
}

inline void USART_vidResetIsDataOverrun(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_A, 3);
}

inline void USART_vidResetIsParityError(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_A, 2);
}

// *** interrupts ***//
inline void USART_vidEnableTxCompleteINT(void)
{
	BIT_SET(USART_CTRL_STATUS_B, 6);
}

inline void USART_vidDisableTxCompleteINT(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_B, 6);
}

inline void USART_vidEnableTxReadyINT(void)
{
	BIT_SET(USART_CTRL_STATUS_B, 5);
}

inline void USART_vidDisableTxReadyINT(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_B, 5);
}

inline void USART_vidEnableRxCompleteINT(void)
{
	BIT_SET(USART_CTRL_STATUS_B, 7);
}

inline void USART_vidDisableRxCompleteINT(void)
{
	BIT_CLEAR(USART_CTRL_STATUS_B, 7);
}

// *** interrupts callbacks ***//
inline void USART_vidRegisterCB_TxComplete(const USART_CB_Tx_t CBfuncCpy, const u8 u8PositionCpy)
{
	if (u8PositionCpy < USART_CB_LENGTH_Tx_COMPLETE)
			USART_CB_Queue_Tx_Complete[u8PositionCpy] = CBfuncCpy;
}

inline void USART_vidDeregisterCB_TxComplete(const u8 u8PositionCpy)
{
	if (u8PositionCpy < USART_CB_LENGTH_Tx_COMPLETE)
			USART_CB_Queue_Tx_Complete[u8PositionCpy] = 0;
}

inline void USART_vidRegisterCB_TxReady(const USART_CB_Tx_t CBfuncCpy, const u8 u8PositionCpy)
{
	if (u8PositionCpy < USART_CB_LENGTH_Tx_READY)
			USART_CB_Queue_Tx_Ready[u8PositionCpy] = CBfuncCpy;
}

inline void USART_vidDeregisterCB_TxReady(const u8 u8PositionCpy)
{
	if (u8PositionCpy < USART_CB_LENGTH_Tx_READY)
			USART_CB_Queue_Tx_Ready[u8PositionCpy] = 0;
}

inline void USART_vidRegisterCB_RxComplete(const USART_CB_Rx_t CBfuncCpy, const u8 u8PositionCpy)
{
	if (u8PositionCpy < USART_CB_LENGTH_Rx_COMPLETE)
			USART_CB_Queue_Rx_Complete[u8PositionCpy] = CBfuncCpy;
}

inline void USART_vidDeregisterCB_RxComplete(const u8 u8PositionCpy)
{
	if (u8PositionCpy < USART_CB_LENGTH_Rx_COMPLETE)
			USART_CB_Queue_Rx_Complete[u8PositionCpy] = 0;
}

// *** operation modes ***//
void USART_vidIsTxSpeed_x2(const u8 u8StateCpy)
{
	USART_vidResetIsFrameError();
	USART_vidResetIsDataOverrun();
	USART_vidResetIsParityError();

	BIT_ASSIGN(USART_CTRL_STATUS_A, 1, u8StateCpy);
}

void USART_vidIsMultiProcessorComm(const u8 u8StateCpy)
{
	USART_vidResetIsFrameError();
	USART_vidResetIsDataOverrun();
	USART_vidResetIsParityError();

	BIT_ASSIGN(USART_CTRL_STATUS_A, 0, u8StateCpy);
}

void USART_vidChangeDataSize(USART_data_sz_t enumDataSzCpy)
{
	 // 3rd bit must be written to 'USART_CTRL_STATUS_B.pin2'
	BIT_ASSIGN( USART_CTRL_STATUS_B, 2, BIT_GET(enumDataSzCpy, 2) );

	USART_vidChangeBit_USART_CTRL_STATUS_C(2, BIT_GET(enumDataSzCpy, 1));
	USART_vidChangeBit_USART_CTRL_STATUS_C(1, BIT_GET(enumDataSzCpy, 0));
}

inline void USART_vidChangeSynchMode(USART_synch_t enumUSARTsynchModeCpy)
{
	USART_vidChangeBit_USART_CTRL_STATUS_C(6, enumUSARTsynchModeCpy);
}

inline void USART_vidChangeParityMode(USART_parity_t enumParityModeCpy)
{
	USART_vidChangeBit_USART_CTRL_STATUS_C(5, BIT_GET(enumParityModeCpy, 1));
	USART_vidChangeBit_USART_CTRL_STATUS_C(4, BIT_GET(enumParityModeCpy, 0));
}

inline void USART_vidChangeStopBitMode(USART_stopBit_t enumStopBitSzCpy)
{
	USART_vidChangeBit_USART_CTRL_STATUS_C(3, enumStopBitSzCpy);
}

inline void USART_vidChangeBaudRate(USART_baud_clk_t enumBaudClkCpy)
{
	USART_BAUD_RATE_LBYTE                   = (u8)enumBaudClkCpy;
	USART_CTRL_STATUS_C_AND_BAUD_RATE_HBYTE = (u8)((enumBaudClkCpy >> 8) & 0b01111111);
}
// ---------------------//


// ------ getters ------//
u16 USART_u16GetRxValue(void)
{
	// trap until there are new data (while no new data)
	while (!USART_u8isRxBufferNewData())
	{}

	register u16 retVal = 0;

	if (USART_u8is9bitData()) // if 9-bit data
	{
		retVal = BIT_GET(USART_CTRL_STATUS_B, 1); // get 9th bit from the register
		retVal <<= 8;
	}

	retVal |= USART_RX_TX_DATA;

	// if Rx Complete INT is disabled
	if (!USART_u8isRxCompleteINTenabled())
	{
	    BIT_SET(USART_CTRL_STATUS_A, 7); // clear 'Receive Complete' bit by writing 1 to it
	}

	return retVal;
}

inline u8 USART_u8isTxBufferFinished(void) // is all data shifted/transmitted
{
	return BIT_GET(USART_CTRL_STATUS_A, 6);
}

inline u8 USART_u8isTxReadyToAcceptData(void)
{
	return BIT_GET(USART_CTRL_STATUS_A, 5);
}

inline u8 USART_u8isRxBufferNewData(void) // not all data was read
{
	return BIT_GET(USART_CTRL_STATUS_A, 7);
}

inline u8 USART_u8isTxCompleteINTenabled(void)
{
	return BIT_GET(USART_CTRL_STATUS_B, 6);
}

inline u8 USART_u8isTxReadyINTenabled(void)
{
	return BIT_GET(USART_CTRL_STATUS_B, 5);
}

inline u8 USART_u8isRxCompleteINTenabled(void)
{
	return BIT_GET(USART_CTRL_STATUS_B, 7);
}

inline u8 USART_u8isFrameError(void)
{
	return BIT_GET(USART_CTRL_STATUS_A, 4);
}

inline u8 USART_u8isDataOverrun(void)
{
	return BIT_GET(USART_CTRL_STATUS_A, 3);
}

inline u8 USART_u8isParityError(void)
{
	return BIT_GET(USART_CTRL_STATUS_A, 2);
}
// ---------------------//


ISR(USART_TXC_vect) // called when Tx finishes transmitting
{
	for (u8 i = 0; i < USART_CB_LENGTH_Tx_COMPLETE; i++)
	{
		if (USART_CB_Queue_Tx_Complete[i])
			USART_CB_Queue_Tx_Complete[i]();
	}

    BIT_SET(USART_CTRL_STATUS_A, 6); // clear 'Transmit Complete' bit by writing 1 to it
}

ISR(USART_UDRE_vect) // called when the 'UDRE' bit (bit5) in 'USART_CTRL_STATUS_A' is set (Tx Ready interrupt)
{
	for (u8 i = 0; i < USART_CB_LENGTH_Tx_READY; i++)
	{
		if (USART_CB_Queue_Tx_Ready[i])
			USART_CB_Queue_Tx_Ready[i]();
	}
}

ISR(USART_RXC_vect) // called when Rx finishes receiving
{
	// if this value is not read immediately, the 'Receive Complete' bit (USART_CTRL_STATUS_A.bit7) will stay 1,
	// resulting in an infinite callback-functions call (infinite ISR).
	register u16 Rx_val = USART_u16GetRxValue();

	for (u8 i = 0; i < USART_CB_LENGTH_Rx_COMPLETE; i++)
	{
		if (USART_CB_Queue_Rx_Complete[i])
			USART_CB_Queue_Rx_Complete[i](Rx_val);
	}

    BIT_SET(USART_CTRL_STATUS_A, 7); // clear 'Receive Complete' bit by writing 1 to it,
                                     // meaning that all data were read from Rx buffer.
}

