/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MCAL_DRIVERS_USART_DRIVER_USART_FUNCTIONS_H_
#define MCAL_DRIVERS_USART_DRIVER_USART_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

// baud rate and clock freq.
typedef enum
{
	// F_CPU = 8MHz
	USART_Baud_rate_8MHz_2400   = 207,
	USART_Baud_rate_8MHz_4800   = 103,
	USART_Baud_rate_8MHz_9600   = 51,
	USART_Baud_rate_8MHz_14400  = 34,
	USART_Baud_rate_8MHz_19200  = 25,
	USART_Baud_rate_8MHz_28800  = 16,
	USART_Baud_rate_8MHz_38400  = 12,
	USART_Baud_rate_8MHz_57600  = 8,
	USART_Baud_rate_8MHz_76800  = 6,
	USART_Baud_rate_8MHz_115200 = 3,

	// F_CPU = 16MHz
	USART_Baud_rate_16MHz_2400  = 416,
	USART_Baud_rate_16MHz_4800  = 207,
	USART_Baud_rate_16MHz_9600  = 103,
	USART_Baud_rate_16MHz_14400 = 68,
	USART_Baud_rate_16MHz_19200 = 51,
	USART_Baud_rate_16MHz_28800 = 34,
	USART_Baud_rate_16MHz_38400 = 25,
	USART_Baud_rate_16MHz_57600 = 16,
	USART_Baud_rate_16MHz_76800 = 12,
    USART_Baud_rate_16MHz_115200 = 8
} USART_baud_clk_t;

// data size
typedef enum
{
	USART_data_size_5bits = 0b00000000,
	USART_data_size_6bits = 0b00000001,
	USART_data_size_7bits = 0b00000010,
	USART_data_size_8bits = 0b00000011,
	USART_data_size_9bits = 0b00000111
} USART_data_sz_t;

// sync mode
typedef enum
{
	USART_asynch = 0,
	USART_synch  = 1
} USART_synch_t;

// parity mode
typedef enum
{
	USART_parity_disabled = 0b00000000,
	USART_parity_even     = 0b00000010,
	USART_parity_odd      = 0b00000011
} USART_parity_t;

// stop bit size
typedef enum
{
	USART_stopBit_1bit  = 0,
	USART_stopBit_2bits = 1
} USART_stopBit_t;

typedef void (*USART_CB_Tx_t)(void); // function pointer for Tx callbacks
typedef void (*USART_CB_Rx_t)(u16);  // function pointer for Rx callbacks

void USART_vidInit(USART_data_sz_t enumDataSzCpy, USART_parity_t enumParityModeCpy,
                   USART_stopBit_t enumStopBitSzCpy,USART_baud_clk_t enumBaudClkCpy,
                   USART_synch_t enumUSARTsynchModeCpy);


void USART_vidTransmitData(const u16 u16ValueCpy);
void USART_vidTransmitStr(const char* charPtrStrCpy);
void USART_vidTransmitInt(s32 s32NumCpy);
void USART_vidTransmitFloat(f32 f32NumCpy, u8 u8AccuracyCpy);
void USART_vidTransmitDataAsBits(const u16 u16NumCpy);


// ------ setters ------ //
// *** buffers ***//
void USART_vidEnableTx(void);
void USART_vidDisableTx(void);

void USART_vidEnableRx(void);
void USART_vidDisableRx(void);

// *** error flags ***//
void USART_vidResetIsFrameError(void);
void USART_vidResetIsDataOverrun(void);
void USART_vidResetIsParityError(void);

// *** interrupts ***//
void USART_vidEnableTxCompleteINT(void);
void USART_vidDisableTxCompleteINT(void);

void USART_vidEnableTxReadyINT(void);
void USART_vidDisableTxReadyINT(void);

void USART_vidEnableRxCompleteINT(void);
void USART_vidDisableRxCompleteINT(void);

// *** interrupts callbacks ***//
void USART_vidRegisterCB_TxComplete(const USART_CB_Tx_t CBfuncCpy, const u8 u8PositionCpy);
void USART_vidDeregisterCB_TxComplete(const u8 u8PositionCpy);

void USART_vidRegisterCB_TxReady(const USART_CB_Tx_t CBfuncCpy, const u8 u8PositionCpy);
void USART_vidDeregisterCB_TxReady(const u8 u8PositionCpy);

void USART_vidRegisterCB_RxComplete(const USART_CB_Rx_t CBfuncCpy, const u8 u8PositionCpy);
void USART_vidDeregisterCB_RxComplete(const u8 u8PositionCpy);

// *** operation modes ***//
void USART_vidIsTxSpeed_x2(const u8 u8StateCpy);

void USART_vidIsMultiProcessorComm(const u8 u8StateCpy);

void USART_vidChangeDataSize(USART_data_sz_t enumDataSzCpy);
void USART_vidChangeSynchMode(USART_synch_t enumUSARTsynchModeCpy);
void USART_vidChangeParityMode(USART_parity_t enumParityModeCpy);
void USART_vidChangeStopBitMode(USART_stopBit_t enumStopBitSzCpy);
void USART_vidChangeBaudRate(USART_baud_clk_t enumBaudClkCpy);
// ---------------------//


// ------ getters ------//
u16 USART_u16GetRxValue(void);
u8 USART_u8isTxBufferFinished(void); // is all data shifted/transmitted
u8 USART_u8isTxReadyToAcceptData(void);
u8 USART_u8isRxBufferNewData(void); // not all data was read

u8 USART_u8isTxCompleteINTenabled(void);
u8 USART_u8isTxReadyINTenabled(void);
u8 USART_u8isRxCompleteINTenabled(void);

u8 USART_u8isFrameError(void);
u8 USART_u8isDataOverrun(void);
u8 USART_u8isParityError(void);
// ---------------------//


#endif /* MCAL_DRIVERS_USART_DRIVER_USART_FUNCTIONS_H_ */

